#lang racket/base
(provide part-01 part-02 main timed)
(require "lib/common.rkt"
         racket/list racket/function)

(define (part-01 input)
  (define d (read-forest input))
  (define d* (transpose d))
  (define left/right (map visibility d))
  (define up/down (transpose (map visibility d*)))
  (for/sum ([row (in-list left/right)]
            [col (in-list up/down)])
    (count identity (combine-visibile row col))))

(define (char->digit c)
  (- (char->integer c) 48))

(define (transpose xs)
  (apply map list xs))

;; #t for each tree visible (from either end)
(define (visibility trees)
  (combine-visibile (reverse (monotonic trees))
          (monotonic (reverse trees))))

(define (combine-visibile a b)
  (for/list ([l (in-list a)]
             [r (in-list b)])
    (or l r)))

;; #t for each value in the list that is monotonically increasing
;;  1  2  5  3  0  6
;; #t #t #t #f #f #t
(define (monotonic trees)
  (for/fold ([visible null]
             [tallest -1]
             #:result visible)
            ([t (in-list trees)])
    (cond [(> t tallest) (values (cons #t visible) t)]
          [else (values (cons #f visible) tallest)])))

(define (read-forest input)
  (for/list ([l (in-list input)])
    (map char->digit (string->list l))))

(define (part-02 input)
  (define d (read-forest input))
  (define d* (transpose d))
  (define left/right (map scenic-score d))
  (define up/down (transpose (map scenic-score d*)))
  (for/fold ([hut 0])
            ([row (in-list left/right)]
             [col (in-list up/down)])
    (apply max hut (map * row col))))

(define (scenic-score trees)
  (define v (list->vector trees))
  (define left  (viewing-distances v look-left))
  (define right (viewing-distances v look-right))
  (map * left right))

(define (viewing-distances tv f)
  (for/fold ([counts null]
             #:result (reverse counts))
            ([i (in-range (vector-length tv))])
    (cons (f tv i) counts)))

(define (look-right tv i) (scan-with tv i add1 (vector-length tv)))
(define (look-left tv i) (scan-with tv i sub1 -1))

(define (scan-with tv i next stop)
  (define (scan hut j total)
    (cond [(= j stop) total]
          [(>= (vector-ref tv j) hut) (add1 total)]
          [else (scan hut (next j) (add1 total))]))
  (scan (vector-ref tv i) (next i) 0))

;; =========================================================
;; =========================================================
;; =========================================================
(define input (read-input))
(define sample-input (lines
#<<SAMPLE
30373
25512
65332
33549
35390
SAMPLE
))

(define (main [mode 'ignore])
  (case mode
    [(timed)
     (displayln (elapsed-ns/gc (part-01 input)))
     (displayln (elapsed-ns/gc (part-02 input)))]
    [(untimed)
     (displayln (part-01 input))
     (displayln (part-02 input))]
    [else (void)]))
(define (timed) (main 'timed))

(module+ main
  (let* ([args (current-command-line-arguments)])
    (cond [(zero? (vector-length args)) (main)]
          [else (main (string->symbol (vector-ref args 0)))])))

(module+ test
  (require rackunit)

  (check-= (part-01 sample-input) 21 0)
  (check-= (part-02 sample-input) 8 0)

  ;; regression tests
  (check-= (part-01 input) 1809 0)
  (check-= (part-02 input) 479400 0)

  )
