# AoC 2022 Racket

My solutions to [Advent of Code 2022](https://adventofcode.com/2022) using [Racket](https://racket-lang.org/)

1. bin/timings runs all solutions and times them
2. bin/make-day [N] creates a new directory called dayN with:

    * a copy of template.rkt name dayN.rkt
    * a dayN.input file for your input - uses the advent-of-code package to retrieve your personal input file.
    * a dayN.question file for reference/notes
    * a link to the lib directory for reading input

Run both from the root of the repository
