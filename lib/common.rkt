#lang racket/base
(require (only-in racket/string string-split string-replace)
         (only-in racket/file file->string file->lines file->list)
         racket/undefined
         elapsed-nanoseconds
         )

(provide (all-defined-out)
         elapsed-ns elapsed-ns/gc undefined)

;; https://stackoverflow.com/questions/16842811/racket-how-to-retrieve-the-path-of-the-running-file
;; Eli Barzilay's solution
(require racket/runtime-path)
(define-runtime-path lib-path ".")

;; convert a string into a list of numbers
(define (signed-integers line)
  (map string->number (regexp-match* #px"[+-]?\\d+" line)))

(define (integers line)
  (map string->number (regexp-match* #px"\\d+" line)))

;; split a line (or several lines on spaces/newline/tab etc.
(define (words line)
  (regexp-split #px"\\s" line))

;; split input into list of lines
(define (lines s)
  (string-split s "\n"))

;; split input into groups split by a empty lines
(define (groups s)
  (string-split s "\n\n"))

;; Name of input file is daynn.input
;; folder structure is  <repo-path>/year/daynn/lib/
;;   lib is a soft-link to <repo-path>/aoclib
(define (read-input [input-name
                     (regexp-replace #px"(.*)/(day\\d\\d)(/lib/.)"
                                     (path->string lib-path)
                                     "\\1/\\2/\\2.input")])
  (file->lines input-name))

(define (map-input f [input-name
                      (regexp-replace #px"(.*)(/day\\d\\d)(/lib/.)"
                                      (path->string lib-path)
                                      "\\1\\2/\\2.input")])
  (with-input-from-file input-name
    (λ () (for/list ([line (in-lines)]) (f line)))
    #:mode 'text))



(define (read-input-string
         [input-name
          (regexp-replace #px"(.*)(/day\\d\\d)(/lib/.)"
                          (path->string lib-path)
                          "\\1\\2/\\2.input")])
  (file->string input-name))
