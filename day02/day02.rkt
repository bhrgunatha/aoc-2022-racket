#lang racket/base
(require "lib/common.rkt"
         racket/string racket/match)

(define (part-01 input)
  (for/sum ([choices (in-list input)])
    (match-define (list (app shape elf)
                        (app shape self)) (string-split choices))
    (game-score elf self)))

(define (shape token)
  (case token
    [("A" "X") 'rock]
    [("B" "Y") 'paper]
    [("C" "Z") 'scissors]))

(define (game-score elf self)
  (+ (shape-score self)
     (match* (elf self)
       [(same      same)      3]
       [('scissors 'rock)     6]
       [('rock     'paper)    6]
       [('paper    'scissors) 6]
       [(_ _)                 0])))

(define (shape-score shape)
  (case shape
    [(rock)     1]
    [(paper)    2]
    [(scissors) 3]))

(define (outcome-score elf self)
  (case self
    [(rock)     (+ 0 (shape-score (lose elf)))]
    [(paper)    (+ 3 (shape-score       elf))]
    [(scissors) (+ 6 (shape-score (win  elf)))]))

(define (lose elf)
  (case elf
    [(rock)     'scissors]
    [(scissors) 'paper]
    [(paper)    'rock]))

(define (win elf)
  (case elf
    [(rock)     'paper]
    [(scissors) 'rock]
    [(paper)    'scissors]))

(define (part-02 input)
  (for/sum ([choices (in-list input)])
    (match-define (list (app shape elf)
                        (app shape self)) (string-split choices))
    (outcome-score elf self)))


;; =========================================================
;; =========================================================
;; =========================================================
(define input (read-input))
(define sample-input (lines
#<<SAMPLE
A Y
B X
C Z
SAMPLE
))

(module+ test
  (require rackunit)
  (check-= (part-01 sample-input) 15 0)
  (check-= (part-02 sample-input) 12 0)
  ;; regression tests
  (check-= (part-01 input) 12276 0)
  (check-= (part-02 input) 9975 0))


;; speedups
;; --------

;; ;; initial approach
;; (nstime
;;  10
;;  (for/fold ([p1 0]
;;             [p2 0]
;;             #:result (list p1 p2))
;;            ([choices (in-list (read-input))])
;;    (match-define (list (app shape elf)
;;                        (app shape self)) (string-split choices))
;;    (values (+ p1 (game-score elf self))
;;            (+ p2 (outcome-score elf self)))))
;;
;; ;; 1st speedup
;; (require racket/function)
;; (nstime 10
;;  (with-input-from-file "day02.input"
;;    (thunk
;;     (for/fold ([part1 0]
;;                [part2 0]
;;                #:result (list part1 part2))
;;               ([choices (in-lines)])
;;       (values (+ part1
;;                  (case choices
;;                    [("A X") 4] [("A Y") 8] [("A Z") 3]
;;                    [("B X") 1] [("B Y") 5] [("B Z") 9]
;;                    [("C X") 7] [("C Y") 2] [("C Z") 6]))
;;               (+ part2
;;                  (case choices
;;                    [("A X") 3] [("A Y") 4] [("A Z") 8]
;;                    [("B X") 1] [("B Y") 5] [("B Z") 9]
;;                    [("C X") 2] [("C Y") 6] [("C Z") 7])))))))
;;
;; ;; 2nd
;; (nstime 10
;;  (with-input-from-file "day02.input"
;;    (thunk
;;     (define scores
;;       (make-immutable-hash
;;        '(("A X" . (4 3)) ("A Y" . (8 4)) ("A Z" . (3 8))
;;          ("B X" . (1 1)) ("B Y" . (5 5)) ("B Z" . (9 9))
;;          ("C X" . (7 2)) ("C Y" . (2 6)) ("C Z" . (6 7)))))
;;     (for/fold ([part1 0]
;;                [part2 0]
;;                #:result (list part1 part2))
;;               ([choices (in-lines)])
;;       (define wat (hash-ref scores choices))
;;       (values (+ part1 (car wat))
;;               (+ part2 (cadr wat)))))))
