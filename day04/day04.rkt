#lang racket/base
(require "lib/common.rkt"
         racket/match
         racket/list)

(define (part-01 input)
  (count contained? input))

(define (assignments elves)
  (map string->number (regexp-split #rx"[-,]" elves)))

(define (contained? elves)
  (match-define (list min/a max/a min/b max/b) (assignments elves))
  (or (and (<= min/a min/b) (>= max/a max/b))
      (and (<= min/b min/a) (>= max/b max/a))))

(define (part-02 input)
  (count overlap? input))

(define (overlap? elves)
  (match-define (list min/a max/a min/b max/b) (assignments elves))
  (and (>= max/a min/b) (>= max/b min/a)))

;; =========================================================
;; =========================================================
;; =========================================================
(define input (read-input))
(define sample-input (lines
#<<SAMPLE
2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8
SAMPLE
))

(module+ test
  (require rackunit)
  (check-= (part-01 sample-input) 2 0)
  (check-= (part-02 sample-input) 4 0)
  ;; regression tests
  (check-= (part-01 input) 573 0)
  (check-= (part-02 input) 867 0)

  )
