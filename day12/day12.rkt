#lang racket/base
(provide part-01 part-02 main)
(require "lib/common.rkt"
         racket/match
         racket/set
         data/queue)

(define (part-01 input)
  (define-values (heights start end) (parse-mountain input))
  (shortest-path start (higher+1 heights) (found? (set end))))

(define (parse-mountain input)
  (for*/fold ([heights (hash)]
              [start #f]
              [end #f])
             ([(l row) (in-indexed (in-list input))]
              [(c col) (in-indexed (in-string l))])
    (define p (list row col))
    (cond [(eq? #\S c) (values (hash-set heights p -1) p end)]
          [(eq? #\E c) (values (hash-set heights p 26) start p)]
          [else (values (hash-set heights p (- (char->integer c) (char->integer #\a)))
                        start end)])))

(define ((found? targets) p) (set-member? targets p))

(define (path-size steps end [size 0])
  ;; result is (sub1 size) because final iteration (@ start) sets
  ;; - next := steps[start] = #f
  ;; - size := size + 1
  (for/fold ([p end] [size size] #:result (sub1 size))
            ([i (in-naturals 1)] #:break (not p))
    (define next (hash-ref steps p))
    (values next (add1 size))))

(define (shortest-path start next stop?)
  (define (bfs)
    (cond [(queue-empty? Q) #f]
          [else
           (define S (dequeue! Q))
           (match-define (cons current from) S)
           (cond [(stop? current) (hash-set! steps current from)
                                  (path-size steps current)]
                 [else (for ([next (in-list (next current))]
                             #:unless (hash-has-key? steps next))
                         (enqueue! Q (cons next current))
                         (hash-set! steps next current))
                       (bfs)])]))
  (define Q (make-queue))
  (enqueue! Q (cons start #f))
  (define steps (make-hash `((,start . #f))))
  (bfs))

(match-define (list N S E W)
  '((-1 0) (1 0) (0 1) (0 -1)))

(define ((higher+1 heights) p)
  (define h (hash-ref heights p))
  (for/list ([n (in-list (list (map + p N)
                               (map + p S)
                               (map + p E)
                               (map + p W)))]
             #:when (<= (- (hash-ref heights n +inf.0) h) 1))
    n))

(define ((lower-1 heights) p)
  (define h (hash-ref heights p))
  (for/list ([n (in-list (list (map + p N)
                               (map + p S)
                               (map + p E)
                               (map + p W)))]
             #:when (<= (- h (hash-ref heights n -inf.0)) 1))
    n))

(define (part-02 input)
  (define-values (heights start end) (parse-mountain input))
  (define starts
    (for/set ([(k v) (in-hash heights)]
              #:when (zero? v)) k))
  (shortest-path end (lower-1 heights) (found? starts)))


;; =========================================================
;; =========================================================
;; =========================================================
(define input (read-input))
(define sample-input (lines
#<<SAMPLE
Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi
SAMPLE
))

(define (main [mode 'ignore])
  (case mode
    [(timed)
     (displayln (elapsed-ns/gc (part-01 input)))
     (displayln (elapsed-ns/gc (part-02 input)))]
    [(untimed)
     (displayln (part-01 input))
     (displayln (part-02 input))]
    [else (void)]))
(define (timed) (main 'timed))

(module+ main
  (let* ([args (current-command-line-arguments)])
    (cond [(zero? (vector-length args)) (main)]
          [else (main (string->symbol (vector-ref args 0)))])))

(module+ test
  (require rackunit)
  (check-= (part-01 sample-input) 31 0)
  (check-= (part-02 sample-input) 29 0)

  ;; regression tests
  (check-= (part-01 input) 380 0)
  (check-= (part-02 input) 375 0)

  )
