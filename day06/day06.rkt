#lang racket/base
(provide part-01 part-02 main)
(require "lib/common.rkt"
         racket/list
         racket/set)

(define (part-01 input)
  (for/first ([(a i) (in-indexed (in-string input))]
              [b (in-string (substring input 1))]
              [c (in-string (substring input 2))]
              [d (in-string (substring input 3))]
              #:when (= 4 (set-count (set a b c d))))
    (+ i 4)))

(define (part-02 input)
  (let search ([cs (string->list input)]
               [i 0])
    (cond [(= 14 (set-count (list->set (take cs 14))))
           (+ i 14)]
          [else (search (rest cs) (add1 i))])))

;; =========================================================
;; =========================================================
;; =========================================================
(define input (read-input-string))
(define sample-input (lines
#<<SAMPLE
mjqjpqmgbljsphdztnvjfqwrcgsmlb     first marker after character 7
bvwbjplbgvbhsrlpgdmjqwftvncz       first marker after character 5
nppdvjthqldpwncqszvftbrmjlhg       first marker after character 6
nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg  first marker after character 10
zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw   first marker after character 11
SAMPLE
))

(define (main [mode 'ignore])
  (case mode
    [(timed)
     (displayln (elapsed-ns/gc (part-01 input)))
     (displayln (elapsed-ns/gc (part-02 input)))]
    [(untimed)
     (displayln (part-01 input))
     (displayln (part-02 input))]
    [else (void)]))
(define (timed) (main 'timed))

(module+ main
  (let* ([args (current-command-line-arguments)])
    (cond [(zero? (vector-length args)) (main)]
          [else (main (string->symbol (vector-ref args 0)))])))

(module+ test
  (require rackunit)
  (check-equal? (map part-01 sample-input) '(7 5 6 10 11))
  (check-equal? (map part-02 sample-input) '(19 23 23 29 26))

  ;; regression tests
  (check-= (part-01 input) 1702 0)
  (check-= (part-02 input) 3559 0)

  )

(module speedrun racket/base
  (require "lib/common.rkt"
           racket/file)
  (provide speed-test)

  (define (packet-start s n)
    (for/fold ([cs (counts (substring s 0 n))]
               [found? #f]
               #:result found?)
              ([i (in-naturals)]
               [start (in-string s)]
               [end (in-string (substring s n))]
               #:break found?)
      (if (= n (hash-count cs))
          (values cs (+ i n))
          (values (udpate-counts cs start end) found?))))

  (define (udpate-counts counts removal addition)
    (define removed
      (if (<= (hash-ref counts removal 0) 1)
          (hash-remove counts removal)
          (hash-update counts removal sub1)))
    (hash-update removed addition add1 0))

  (define (counts s)
    (for/fold ([cs (hasheqv)])
              ([c (in-string s)])
      (hash-update cs c add1 0)))

  (define (speed-test)
    (elapsed-ns/gc 5
     (define input (file->string "day06.input"))
     (list (packet-start input 4)
           (packet-start input 14))))

  )

;; (require 'speedrun)
;; (speed-test)
