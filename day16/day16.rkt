#lang racket/base
(provide part-01 part-02 main)
(require "lib/common.rkt"
         graph
         racket/set racket/match racket/list)

(define G #f)
(define (part-01 input)
  (set! G (pipe-graph input))
  (define closed
    (for/set ([k (in-hash-keys G)]
              #:unless (eq? k 'AA))
      k))
  (single-flow 'AA 30 closed))

(define (pipe-graph input)
  (define pipes (read-valves input))
  ;; valves with flow (plus 'AA)
  (define valves (connected-valves pipes))
  ;; all distances
  (define dists (distances pipes))
  (for/hash ([u (in-set valves)])
    (values u
            (cons (first (hash-ref pipes u))
                  (for/list ([v (in-list (neighbours dists valves u))])
                    (list v (hash-ref dists (list u v))))))))

(define single-flow
  (let ([memo (make-hash)])
    (λ (u t closed)
      (cond [(set-empty? closed) (hash-ref! memo (list u t closed) 0)]
            [(zero? t) (hash-ref! memo (list u t closed) 0)]
            [else
             (hash-ref!
              memo (list u t closed)
              (for/fold ([max-flow 0])
                        ([vt (in-list (rest (hash-ref G u)))]
                         #:do [(match-define (list v dt) vt)]
                         #:when (< dt (sub1 t))
                         #:when (set-member? closed v))
                (define open-time (- t dt 1))
                (define provides (* (first (hash-ref G v)) open-time))
                (define sub-flow
                  (single-flow v open-time (set-remove closed v)))
                (max max-flow (+ provides sub-flow))))]))))

(define (neighbours dists valves u)
  (for/list ([edge (in-hash-keys dists)]
             #:when (eq? u (first edge))
             #:unless (eq? (first edge) (second edge))
             #:when (set-member? valves (second edge)))
    (second edge)))

(define (connected-valves pipes)
  (set-add
   (for/set ([(k v) (in-hash pipes)] #:when (> (first v) 0)) k)
   'AA))

(define (distances pipes)
  (define es
    (for*/list ([(from x) (in-hash pipes)]
                [to (in-list (rest x))])
      (list 1 from to)))
  (johnson (weighted-graph/directed es)))

(define (read-valves input)
  (for/hasheq ([line (in-list input)])
    (match-define (list* _ from (app string->number flow) tos)
      (regexp-match* #px"[A-Z]+|\\d+" line))
    (values (string->symbol from) (cons flow (map string->symbol tos)) )))

(define dual-flow
  (let ([memo (make-hash)])
    (λ (u t closed)
      (cond [(set-empty? closed) (hash-ref! memo (list u t closed) 0)]
            [(zero? t) (hash-ref! memo (list u t closed) 0)]
            [else
             (hash-ref!
              memo (list u t closed)
              (max
               (for/fold ([max-flow 0])
                         ([vt (in-list (rest (hash-ref G u)))]
                          #:do [(match-define (list v dt) vt)]
                          #:when (< dt (sub1 t))
                          #:when (set-member? closed v))
                 (define open-time (- t dt 1))
                 (define provides (* (first (hash-ref G v)) open-time))
                 (define sub-flow
                   (dual-flow v open-time (set-remove closed v)))
                 (max max-flow (+ provides sub-flow)))
               (single-flow 'AA 26 closed)))]))))

(define (part-02 input)
  (set! G (pipe-graph input))
  (define closed
    (for/seteq ([k (in-hash-keys G)]
                #:unless (eq? k 'AA))
      k))
  (dual-flow 'AA 26 closed))

;; =========================================================
;; =========================================================
;; =========================================================
(define input (read-input))
(define sample-input (lines
#<<SAMPLE
Valve AA has flow rate=0; tunnels lead to valves DD, II, BB
Valve BB has flow rate=13; tunnels lead to valves CC, AA
Valve CC has flow rate=2; tunnels lead to valves DD, BB
Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE
Valve EE has flow rate=3; tunnels lead to valves FF, DD
Valve FF has flow rate=0; tunnels lead to valves EE, GG
Valve GG has flow rate=0; tunnels lead to valves FF, HH
Valve HH has flow rate=22; tunnel leads to valve GG
Valve II has flow rate=0; tunnels lead to valves AA, JJ
Valve JJ has flow rate=21; tunnel leads to valve II
SAMPLE
                      ))

(define (main [mode 'ignore])
  (case mode
    [(timed)
     (displayln (elapsed-ns/gc (part-01 input)))
     (displayln (elapsed-ns/gc (part-02 input)))]
    [(untimed)
     (displayln (part-01 input))
     (displayln (part-02 input))]
    [else (void)]))
(define (timed) (main 'timed))

(module+ main
  (let* ([args (current-command-line-arguments)])
    (cond [(zero? (vector-length args)) (main)]
          [else (main (string->symbol (vector-ref args 0)))])))

(module+ test
  (require rackunit)

  )
